//package cn.tian.aop;
//
//import org.springframework.stereotype.Component;
//
///**
// * @author tcy
// * @Date 14-03-2022
// */
//
//@Aspect
//@Component
//public class LogInterceptor {
//
//	@Pointcut("execution(* com.joonwhee.open.service..*.*(..))")
//	public void pointcut() {
//	}
//
//	@Around("pointcut()")
//	public Object around(ProceedingJoinPoint pjp) throws Throwable {
//		Object[] args = pjp.getArgs();
//		try {
//			long start = System.currentTimeMillis();
//			Object result = pjp.proceed();
//			return result;
//		} catch (Exception e) {
//			return "";
//		}
//	}
//
//
//}