package cn.tian.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

/**
 * @author tcy
 * @Date 07-03-2022
 */
@Component
public class MyEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new ApplicationEvent.
	 *
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	public MyEvent(Object source) {
		super(source);
	}


}