package cn.tian.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author tcy
 * @Date 07-03-2022
 */

@Component
public class MyEventListener implements ApplicationListener<MyEvent> {

	@EventListener
	@Override
	public void onApplicationEvent(MyEvent event) {
		Object msg = event.getSource();
		System.out.println("自定义事件监听器（MyEventListener1）收到发布的消息: " + msg);

	}
}