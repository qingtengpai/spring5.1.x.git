大家好，我是程序员田同学。今天带大家用最简单和轻量级的方式构建spirng源码！

作为一名，不想只成为增删改查的boy而言，阅读spirng源码就是面前的一座大山，阅读源码，首先要从构建源码开始，要不然是不能加注释的。

在网上搜了一堆构建方法，都是各个文章抄来抄去，构建了几次都没能成功。网上的方法无非都是，下载grandle、配置、下载源码、bulid...安装一个grandle就让人身心俱疲，而且在这个过程会遇到各种各样的坑。

本方法目测是最轻量级和最简单的方法了。

本方法采用项目内置的grandle进行源码构建，Java开发使用maven更多一点，为了构建源码，再单独下载grandle着实性价比不高。

### 拉取源代码

从源仓库拉取实在是太慢了，我在我自己的码云库提交了一个源代码版本，源代码中并有我自己学习过程中的一些注释，会持续更新。

[spirng源码下载](https://gitee.com/qingtengpai/spring5.1.x.git)

### 配置grandle

拉取下来后不要急着idea打开，修改一下grandle的镜像地址，改为国内地址，要不然你可能到下班都没能把源码构建好。

在项目文件中找到build.gradle文件，修改其中的repositories

```
 repositories {
    maven{ url http://maven.aliyun.com/nexus/content/groups/public/}
    maven{ url http://maven.aliyun.com/nexus/content/repositories/jcenter}
  }
```

然后idea打开源码，idea会自动加载依赖，喝杯茶简单的等待二分钟，点击右侧的grandle-build。

### 测试代码编写

创建一个model选择grandle创建完成以后会自动打开build.grandle文件，在dependencies增加一下配置。

```
    compile(project(":spring-context"));
    compile(project(":spring-core"));
```

这个步骤会引用我们自己本地构建的spring源码。

以下是我自己编写的简单的测试代码，目的是看能不能正常启动。

![image-20220113101153406](https://gitee.com/qingtengpai/oss/raw/master/img/image-20220113101153406.png)

![image-20220113101315832](https://gitee.com/qingtengpai/oss/raw/master/img/image-20220113101315832.png)

![image-20220113101250416](https://gitee.com/qingtengpai/oss/raw/master/img/image-20220113101250416.png)

好啦，完全没有任何问题。



